import React, { PureComponent } from 'react'
import { View, StatusBar } from 'react-native'
import { Container } from '../components/Container'
import { Logo } from '../components/Logo'
import { InputNormal, TextNormal } from '../components/TextInput'

const TEMP_BASE_CURRENCY = 'USD'
const TEMP_QUOTE_CURRENCY = 'GBP'
const TEMP_BASE_PRICE = '100'
const TEMP_QUOTE_PRICE = '79.74'

class Login extends PureComponent {

  _buttonClicked = () => {
    console.log('Button Clicked');

  }
  render(){
    return(
      <Container>
        <StatusBar transculent={false} barStyle="light-content"/>
        <Logo/>
        <InputNormal
          buttonText={TEMP_BASE_CURRENCY}
          placeHolder={'Username'}
        />
        <InputNormal
          buttonText={TEMP_QUOTE_CURRENCY}
          placeHolder={'Password'}
          editable={false}/>
        <TextNormal
          text={'LOGIN'}
          onPress={this._buttonClicked}/>
        <View/>
      </Container>
    )
  }
}

export default Login