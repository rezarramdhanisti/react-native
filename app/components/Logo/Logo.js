import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from './styles'

const Logo = () => (
  <View style={styles.container}>
    <Image
      resizeMode = "contain"
      style = {styles.containerImage}
      source={require('./logo.jpg')}/>
    <Text style={styles.text}>Hangar Parking Managment</Text>
  </View>
)

export default Logo