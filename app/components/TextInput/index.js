import InputNormal from './InputNormal'
import TextNormal from './TextNormal'
import styles from './styles'

export { InputNormal, TextNormal, styles }