import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, TextInput, TouchableHighlight, } from 'react-native'

import styles from './styles'

const InputNormal = ({ onPress, buttonText, editable = true, placeHolder} ) => (
  <View  style={styles.container}>
    <View style={styles.border} />
    <TextInput  style={styles.input}
    placeholder= {placeHolder}/>
  </View>
)

InputNormal.propTypes = {
  onPress: PropTypes.func,
  buttonText: PropTypes.string,
  editable: PropTypes.bool
}

export default InputNormal