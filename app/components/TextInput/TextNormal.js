import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, TextInput, TouchableHighlight} from 'react-native'

import styles from './styles'

const TextNormal = ({ onPress, text} ) => (
  <View  style={styles.containerNormal}>
    <Text style={styles.buttonTextNormal} onPress={onPress} >{text}</Text>
  </View>
)

TextNormal.propTypes = {
  onPress: PropTypes.func,
  text: PropTypes.string,
}

export default TextNormal
