import EStyleSheet from 'react-native-extended-stylesheet';
import { StyleSheet } from 'react-native'
import { Dimensions } from 'react-native'

const INPUT_HEIGHT = 48
const BORDER_RADIUS = 4

export default EStyleSheet.create({
  container: {
    backgroundColor: '$white',
    width: '90%',
    height: INPUT_HEIGHT,
    borderRadius: BORDER_RADIUS,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 11,
    borderWidth: 1.5,
    borderColor: '$black',
  },
  containerNormal: {
    width: 80,
    height: INPUT_HEIGHT,
  },
  buttonContainer: {
    // height: INPUT_HEIGHT,
    // alignItems: 'center',
    // justifyContent: 'center',
    // backgroundColor: '$white',
    // borderTopLeftRadius: BORDER_RADIUS,
    // borderBottomLeftRadius: BORDER_RADIUS
  },
  buttonTextNormal: {
    fontWeight: '600',
    fontSize: 24,
    letterSpacing: 2,
    marginTop: 20,
  },
  buttonText: {
    fontWeight: '600',
    fontSize: 20,
    paddingHorizontal: 16,
    color: '$black'
  },
  input: {
    height: INPUT_HEIGHT,
    flex: 1,
    fontSize: 18,
    paddingHorizontal: 8,
    color: '$black'
  },
  border: {
    height:INPUT_HEIGHT,
    width: StyleSheet.hairlineWidth,
    backgroundColor: '$black'


  },
})