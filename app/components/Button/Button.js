import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'react-native'

import styles from './styles'

const ButtonNormal = ({ onPress, buttonText} ) => (
  <View  style={styles.container}>
    <View style={styles.border} />
    <TextInput  style={styles.input}
    placeholder= {placeHolder}/>
  </View>
)

ButtonNormal.propTypes = {
  onPress: PropTypes.func,
  buttonText: PropTypes.string,
}

export default ButtonNormal