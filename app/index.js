import React from 'react'
import ESyleSheet from 'react-native-extended-stylesheet'

import Login from './screens/Login'

ESyleSheet.build({
  $primaryBlue: '#FFFFFF',
  $black: '#000000',
  $white: '#FFFFFF',
  $inputText: '#797979'
})

export default () => <Login/>